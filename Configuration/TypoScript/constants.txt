plugin.tx_mooxmarketplaceclassifieds {
	settings {
		defaults {
			# cat=plugin.tx_mooxmarketplaceclassifieds/various; type=int+; label=Default value of new filter
			newFilter = 0
		}
	}
}

module.tx_mooxmarketplaceclassifieds {
	settings {
		# cat=module.tx_mooxmarketplaceclassifieds/various; type=int+; label=Backend view category root
		backendCategoryRoot = 0			
	}
}