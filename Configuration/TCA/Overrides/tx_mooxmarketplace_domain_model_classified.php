<?php
defined('TYPO3_MODE') or die();

// set default language file as ll-reference
$extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['moox_marketplace_classifieds']);

// set default language file as ll-reference
$ll = 'LLL:EXT:moox_marketplace_classifieds/Resources/Private/Language/locallang.xml:';

// add new classified variant to variant selector
//$GLOBALS['TCA']['tx_mooxmarketplace_domain_model_classified']['columns']['variant']['config']['items'][] = array('LLL:EXT:moox_marketplace_classifieds/Resources/Private/Language/locallang_db.xlf:tx_mooxmarketplaceclassifieds_domain_model_classified','moox_marketplace_classifieds');

// hide news default fields for new news type 
$hideFields = "";

// set tca array of new moox_marketplace elements
$tx_mooxmarketplaceclassifieds_domain_model_classified = array(
	'moox_marketplace_classifieds_type' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_type',
		'config' => array(
			'type' => 'radio',
			'items' => array(
				array($ll.'form.moox_marketplace_classifieds_type.offer', '1'),
				array($ll.'form.moox_marketplace_classifieds_type.search', '2'),
			),
			'size' => 1,
			'maxitems' => 1,
			'allowNonIdValues' => 1,
			'eval' => 'required',
		),
		// special moox configuration
		'moox' => array(			
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","detail"
				),
			),
			'sortable' => 1,
		),
	),
	'moox_marketplace_classifieds_status' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_status',
		'config' => array(
			'type' => 'radio',
			'items' => array(
				array($ll.'form.moox_marketplace_classifieds_status.private', '1'),
				array($ll.'form.moox_marketplace_classifieds_status.commercial', '2'),
			),
			'size' => 1,
			'maxitems' => 1,
			'allowNonIdValues' => 1,
			'eval' => 'required',
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
						"list","add","edit","detail"
				),
			),
			'sortable' => 1,
		),
	),
	'moox_marketplace_classifieds_imprint_agb' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_imprint_agb',
		'config' => array(
			'type' => 'text',
			'cols' => '40',
			'rows' => '5'
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
	'moox_marketplace_classifieds_price' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_price',
		'config' => array(
			'type' => 'input',
			'size' => '8',
			'max' => '13',
			'eval'     => 'double2',
			'checkbox' => '0',
			'default'  => '0'
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 1,
		),
	),
	'moox_marketplace_classifieds_price_additional_info' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_price_additional_info',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array($ll.'form.moox_marketplace_classifieds_price_additional_info.value0', '0'),
				array($ll.'form.moox_marketplace_classifieds_price_additional_info.value1', '1'),
				array($ll.'form.moox_marketplace_classifieds_price_additional_info.value2', '2'),
			),
			'size' => 1,
			'maxitems' => 1,
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
	'moox_marketplace_classifieds_shipping' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_shipping',
		'config' => array(
			'type' => 'input',
			'size' => '8',
			'max' => '13',
			'eval'     => 'double2',
			'checkbox' => '0',
			'default'  => '0'
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
	'moox_marketplace_classifieds_shipping_additional_info' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_shipping_additional_info',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array($ll.'form.moox_marketplace_classifieds_shipping_additional_info.value0', '0'),
				array($ll.'form.moox_marketplace_classifieds_shipping_additional_info.value1', '1'),
                array($ll.'form.moox_marketplace_classifieds_shipping_additional_info.value2', '2'),
                array($ll.'form.moox_marketplace_classifieds_shipping_additional_info.value3', '3'),
			),
			'size' => 1,
			'maxitems' => 1,
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
	'moox_marketplace_classifieds_prices_vat' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_prices_vat',
		'config' => array(
				'type' => 'check',
				'default' => 0,
				'size' => 1,
				'maxitems' => 1,
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
	'moox_marketplace_classifieds_contact' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_contact',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'required',
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),			
			'sortable' => 0,
		),		
	),
	'moox_marketplace_classifieds_contact_mail' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_contact_mail',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'required',
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
	'moox_marketplace_classifieds_contact_city' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_contact_city',
		'config' => array(
			'type' => 'input',
			'size' => 30,
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 1,
		),
	),
	'moox_marketplace_classifieds_contact_zip' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_contact_zip',
		'config' => array(
			'type' => 'input',
			'size' => 5,
			'max' => 5,
			'eval' => 'int'
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 1,
		),
	),
	'moox_marketplace_classifieds_contact_street' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_contact_street',
		'config' => array(
			'type' => 'input',
			'size' => 30,
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
	'moox_marketplace_classifieds_contact_telephone' => array(
		'exclude' => 1,
		'displayCond' => 'FIELD:variant:=:moox_marketplace',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => $ll.'form.moox_marketplace_classifieds_contact_telephone',
		'config' => array(
			'type' => 'input',
			'size' => 30,
		),
		// special moox configuration
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"list","add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
	'moox_marketplace_classifieds_files' => array(
		'exclude' => 0,
		'label' => $ll.'form.moox_marketplace_classifieds_files',
		'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
			'moox_marketplace_classifieds_files',
			array(
				'reference' => 'file',
				'maxitems' => 1,
				'maxfilesize' => 20480,
				'accepts' => 'pdf',
				'uploadfolder' => ($extConf['uploadFolder']!="")?$extConf['uploadFolder']:'uploads/tx_mooxmarketplace',
				'appearance' => array(
					'headerThumbnail' => array(
						'width' => '100',
						'height' => '100',
					),
					'createNewRelationLinkTitle' => 'Datei hinzufügen',
				),
				// custom configuration for displaying fields in the overlay/reference table
				// to use the imageoverlayPalette instead of the basicoverlayPalette
				'foreign_types' => array(
					'0' => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					),
					\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
						'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
					)
				),
			),
			'pdf'
		),
		// special moox configuration		
		'moox' => array(
			'extkey' => 'moox_marketplace_classifieds',
			'variant' => 'moox_marketplace',
			'plugins' => array(
				"mooxmarketplace" => array(
					"add","edit","detail"
				),
			),
			'sortable' => 0,
		),
	),
);

// extend moox_marketplace tca with new fields from this extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_mooxmarketplace_domain_model_classified', $tx_mooxmarketplaceclassifieds_domain_model_classified,1);

// place new fields in backend form - optionally generate new tab for extended fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_mooxmarketplace_domain_model_classified', 'moox_marketplace_classifieds_type,moox_marketplace_classifieds_status', '', 'after:variant');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_mooxmarketplace_domain_model_classified', 'moox_marketplace_classifieds_price,moox_marketplace_classifieds_price_additional_info,moox_marketplace_classifieds_shipping,moox_marketplace_classifieds_shipping_additional_info,moox_marketplace_classifieds_prices_vat,moox_marketplace_classifieds_imprint_agb', '', 'after:description');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_mooxmarketplace_domain_model_classified', '--div--;LLL:EXT:moox_marketplace_classifieds/Resources/Private/Language/locallang_db.xlf:tx_mooxmarketplaceclassifieds_domain_model_classified.tabs.contact,moox_marketplace_classifieds_contact,moox_marketplace_classifieds_contact_street,moox_marketplace_classifieds_contact_zip,moox_marketplace_classifieds_contact_city,moox_marketplace_classifieds_contact_mail,moox_marketplace_classifieds_contact_telephone', '', 'after:files');

// hide classified default fields defined in hide fields string 
DCNGmbH\MooxMarketplace\Service\HelperService::tcaHideDefaultFields($hideFields);
?>