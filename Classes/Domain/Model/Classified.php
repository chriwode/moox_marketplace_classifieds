<?php
namespace DCNGmbH\MooxMarketplaceClassifieds\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Dominic Martin <dm@dcn.de>, DCN GmbH
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use \TYPO3\CMS\Extbase\Utility\LocalizationUtility; 
 
/**
 *
 *
 * @package moox_marketplace_classifieds
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Classified extends DCNGmbH\MooxMarketplace\Domain\Model\Classified {
	
	/**
	 * @var \int
	 */
	protected $mooxMarketplaceClassifiedsType;

	/**
	 * @var \int
	 */
	protected $mooxMarketplaceClassifiedsStatus;
	
	/**
	 * @var \string
	 */
	protected $mooxMarketplaceClassifiedsImprintAgb;

	/**
	 * @var \string
	 */
	protected $mooxMarketplaceClassifiedsPrice;

	/**
	 * @var \int
	 */
	protected $mooxMarketplaceClassifiedsPriceAdditionalInfo;

	/**
	 * @var \string
	 */
	protected $mooxMarketplaceClassifiedsShipping;

	/**
	 * @var \int
	 */
	protected $mooxMarketplaceClassifiedsShippingAdditionalInfo;

	/**
	 * @var \int
	 */
	protected $mooxMarketplaceClassifiedsPricesVat;

	/**
	 * @var \string
	 */
	protected $mooxMarketplaceClassifiedsContact;

	/**
	 * @var \string
	 */
	protected $mooxMarketplaceClassifiedsContactMail;

	/**
	 * @var \string
	 */
	protected $mooxMarketplaceClassifiedsContactCity;

	/**
	 * @var \string
	 */
	protected $mooxMarketplaceClassifiedsContactZip;

	/**
	 * @var \string
	 */
	protected $mooxMarketplaceClassifiedsContactStreet;

	/**
	 * @var \string
	 */
	protected $mooxMarketplaceClassifiedsContactTelephone;

	/**	 
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplaceClassifieds\Domain\Model\FileReference>
	 * @lazy
	 */
	protected $mooxMarketplaceClassifiedsFiles;

	/**
	 * Initializes all ObjectStorage properties	
	 *
	 * @return void
	 */
	public function initStorageObjects_mooxMarketplaceClassifieds() {
		$this->mooxMarketplaceClassifiedsFiles = new ObjectStorage();
	}
	
	/**
	 * Get type
	 *
	 * @return integer
	 */
	public function getMooxMarketplaceClassifiedsType($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsType!=""){
				return $this->mooxMarketplaceClassifiedsType;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsType;
		}
	}

	/**
	 * Set type
	 *
	 * @param integer $mooxMarketplaceClassifiedsType type
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsType($mooxMarketplaceClassifiedsType) {
		$this->mooxMarketplaceClassifiedsType = $mooxMarketplaceClassifiedsType;
	}

	/**
	 * Get status
	 *
	 * @return integer
	 */
	public function getMooxMarketplaceClassifiedsStatus($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsStatus!=""){
				return $this->mooxMarketplaceClassifiedsStatus;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsStatus;
		}
	}

	/**
	 * Set status
	 *
	 * @param integer $mooxMarketplaceClassifiedsStatus status
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsStatus($mooxMarketplaceClassifiedsStatus) {
		$this->mooxMarketplaceClassifiedsStatus = $mooxMarketplaceClassifiedsStatus;
	}
	
	/**
	 * Get imprint agb
	 *
	 * @return string
	 */
	public function getMooxMarketplaceClassifiedsImprintAgb($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsImprintAgb!=""){
				return $this->mooxMarketplaceClassifiedsImprintAgb;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsImprintAgb;
		}
	}

	/**
	 * Set imprint agb
	 *
	 * @param string $mooxMarketplaceClassifiedsImprintAgb imprint agb
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsImprintAgb($mooxMarketplaceClassifiedsImprintAgb) {
		$this->mooxMarketplaceClassifiedsImprintAgb = $mooxMarketplaceClassifiedsImprintAgb;
	}
	
	/**
	 * Get price
	 *
	 * @return string
	 */
	public function getMooxMarketplaceClassifiedsPrice($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsPrice!=""){
				return $this->mooxMarketplaceClassifiedsPrice;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsPrice;
		}
	}

	/**
	 * Set price
	 *
	 * @param string $mooxMarketplaceClassifiedsPrice price
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsPrice($mooxMarketplaceClassifiedsPrice) {
		$this->mooxMarketplaceClassifiedsPrice = $mooxMarketplaceClassifiedsPrice;
	}

	/**
	 * Get price additional info
	 *
	 * @return integer
	 */
	public function getMooxMarketplaceClassifiedsPriceAdditionalInfo($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsPriceAdditionalInfo!=""){
				return $this->mooxMarketplaceClassifiedsPriceAdditionalInfo;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsPriceAdditionalInfo;
		}
	}

	/**
	 * Set price additional info
	 *
	 * @param integer $mooxMarketplaceClassifiedsPriceAdditionalInfo price additional info
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsPriceAdditionalInfo($mooxMarketplaceClassifiedsPriceAdditionalInfo) {
		$this->mooxMarketplaceClassifiedsPriceAdditionalInfo = $mooxMarketplaceClassifiedsPriceAdditionalInfo;
	}

	/**
	 * Get shipping
	 *
	 * @return string
	 */
	public function getMooxMarketplaceClassifiedsShipping($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsShipping!=""){
				return $this->mooxMarketplaceClassifiedsShipping;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsShipping;
		}
	}

	/**
	 * Set shipping
	 *
	 * @param string $mooxMarketplaceClassifiedsShipping shipping
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsShipping($mooxMarketplaceClassifiedsShipping) {
		$this->mooxMarketplaceClassifiedsShipping = $mooxMarketplaceClassifiedsShipping;
	}

	/**
	 * Get shipping additional info
	 *
	 * @return integer
	 */
	public function getMooxMarketplaceClassifiedsShippingAdditionalInfo($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsShippingAdditionalInfo!=""){
				return $this->mooxMarketplaceClassifiedsShippingAdditionalInfo;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsShippingAdditionalInfo;
		}
	}

	/**
	 * Set shipping additional info
	 *
	 * @param integer $mooxMarketplaceClassifiedsShippingAdditionalInfo shipping additional info
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsShippingAdditionalInfo($mooxMarketplaceClassifiedsShippingAdditionalInfo) {
		$this->mooxMarketplaceClassifiedsShippingAdditionalInfo = $mooxMarketplaceClassifiedsShippingAdditionalInfo;
	}

	/**
	 * Get prices vat
	 *
	 * @return integer
	 */
	public function getMooxMarketplaceClassifiedsPricesVat($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsPricesVat!=""){
				return $this->mooxMarketplaceClassifiedsPricesVat;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsPricesVat;
		}
	}

	/**
	 * Set prices vat
	 *
	 * @param integer $mooxMarketplaceClassifiedsPricesVat prices vat
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsPricesVat($mooxMarketplaceClassifiedsPricesVat) {
		$this->mooxMarketplaceClassifiedsPricesVat = $mooxMarketplaceClassifiedsPricesVat;
	}

	/**
	 * Get contact
	 *
	 * @return string
	 */
	public function getMooxMarketplaceClassifiedsContact($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsContact!=""){
				return $this->mooxMarketplaceClassifiedsContact;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsContact;
		}
	}

	/**
	 * Set contact
	 *
	 * @param string $mooxMarketplaceClassifiedsContact contact
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsContact($mooxMarketplaceClassifiedsContact) {
		$this->mooxMarketplaceClassifiedsContact = $mooxMarketplaceClassifiedsContact;
	}

	/**
	 * Get contact mail
	 *
	 * @return string
	 */
	public function getMooxMarketplaceClassifiedsContactMail($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsContactMail!=""){
				return $this->mooxMarketplaceClassifiedsContactMail;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsContactMail;
		}
	}

	/**
	 * Set contact mail
	 *
	 * @param string $mooxMarketplaceClassifiedsContactMail contact mail
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsContactMail($mooxMarketplaceClassifiedsContactMail) {
		$this->mooxMarketplaceClassifiedsContactMail = $mooxMarketplaceClassifiedsContactMail;
	}

	/**
	 * Get contact city
	 *
	 * @return string
	 */
	public function getMooxMarketplaceClassifiedsContactCity($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsContactCity!=""){
				return $this->mooxMarketplaceClassifiedsContactCity;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsContactCity;
		}
	}

	/**
	 * Set contact city
	 *
	 * @param string $mooxMarketplaceClassifiedsContactCity contact city
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsContactCity($mooxMarketplaceClassifiedsContactCity) {
		$this->mooxMarketplaceClassifiedsContactCity = $mooxMarketplaceClassifiedsContactCity;
	}

	/**
	 * Get contact zip
	 *
	 * @return string
	 */
	public function getMooxMarketplaceClassifiedsContactZip($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsContactZip!=""){
				return $this->mooxMarketplaceClassifiedsContactZip;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsContactZip;
		}
	}

	/**
	 * Set contact zip
	 *
	 * @param string $mooxMarketplaceClassifiedsContactZip contact zip
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsContactZip($mooxMarketplaceClassifiedsContactZip) {
		$this->mooxMarketplaceClassifiedsContactZip = $mooxMarketplaceClassifiedsContactZip;
	}

	/**
	 * Get contact street
	 *
	 * @return string
	 */
	public function getMooxMarketplaceClassifiedsContactStreet($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsContactStreet!=""){
				return $this->mooxMarketplaceClassifiedsContactStreet;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsContactStreet;
		}
	}

	/**
	 * Set contact street
	 *
	 * @param string $mooxMarketplaceClassifiedsContactStreet contact street
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsContactStreet($mooxMarketplaceClassifiedsContactStreet) {
		$this->mooxMarketplaceClassifiedsContactStreet = $mooxMarketplaceClassifiedsContactStreet;
	}

	/**
	 * Get contact telephone
	 *
	 * @return string
	 */
	public function getMooxMarketplaceClassifiedsContactTelephone($preview = false) {
		// define special return for backend preview
		if($preview){
			if($this->mooxMarketplaceClassifiedsContactTelephone!=""){
				return $this->mooxMarketplaceClassifiedsContactTelephone;
			} else {
				return "";
			}
		} else {
			return $this->mooxMarketplaceClassifiedsContactTelephone;
		}
	}

	/**
	 * Set contact telephone
	 *
	 * @param string $mooxMarketplaceClassifiedsContactTelephone contact telephone
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsContactTelephone($mooxMarketplaceClassifiedsContactTelephone) {
		$this->mooxMarketplaceClassifiedsContactTelephone = $mooxMarketplaceClassifiedsContactTelephone;
	}

	/**
	 * sets files
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplaceClassifieds\Domain\Model\FileReference> $mooxMarketplaceClassifiedsFiles
	 *
	 * @return void
	 */
	public function setMooxMarketplaceClassifiedsNewFiles($mooxMarketplaceClassifiedsFiles) {
		$this->mooxMarketplaceClassifiedsNewFiles = $mooxMarketplaceClassifiedsFiles;
	}
	 
	/**
	 * get files
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DCNGmbH\MooxMarketplaceClassifieds\Domain\Model\FileReference>
	 */
	public function getMooxMarketplaceClassifiedsFiles() {
		return $this->mooxMarketplaceClassifiedsFiles;
	}

	/**
     * adds a file
     *
     * @param \DCNGmbH\MooxMarketplaceClassifieds\Domain\Model\FileReference $file
     *
     * @return void
     */
    public function addMooxMarketplaceClassifiedsFiles(\DCNGmbH\MooxMarketplaceClassifieds\Domain\Model\FileReference $file) {
        $this->mooxMarketplaceClassifiedsFiles->attach($file);
    }	
	
	/**
     * remove a file
     *
     * @param \DCNGmbH\MooxMarketplaceClassifieds\Domain\Model\FileReference $file
     *
     * @return void
     */
    public function removeMooxMarketplaceClassifiedsFiles(\DCNGmbH\MooxMarketplaceClassifieds\Domain\Model\FileReference $file) {
        $this->mooxMarketplaceClassifiedsFiles->detach($file);
    }
}
