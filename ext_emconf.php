<?php
########################################################################
# Extension Manager/Repository config file for ext "moox_marketplace_extender".
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
    'title' => 'MOOX marketplace classifieds',
    'description' => 'Erweiterung von MOOX Marktplatz mit benutzerdefinierten Felder und Filtern',
    'category' => 'plugin',
    'author' => 'Jakub Czyz',
    'author_email' => 'jc@dcn.de',
    'shy' => '',
    'dependencies' => 'moox_marketplace',
    'conflicts' => '',
    'priority' => 'bottom',
    'module' => '',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'author_company' => 'DCN GmbH',
    'version' => '0.9.2',
    'constraints' => array(
        'depends' => array(
            'typo3' => '6.2.3-6.2.99',
            'moox_marketplace' => '7.0.0-7.0.99'
        ),
        'conflicts' => array(),
        'suggests' => array(),
    )
);
