<?php
$extensionClassesPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('moox_marketplace_classifieds') . 'Classes/';

$default = array(
    'Tx_MooxMarketplaceClassifieds_Domain_Model_Classified' => $extensionClassesPath . 'Domain/Model/Classified.php',
);
