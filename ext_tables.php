<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

// add static ts files to template selection (only if static ts code or variables are needed)
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'MOOX marketplace classifieds');

// allow new filter elements on standard pages
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mooxmarketplaceclassifieds_domain_model_filter');
?>