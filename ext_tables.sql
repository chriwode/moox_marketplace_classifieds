#
# Extend table structure for table 'tx_mooxmarketplace_domain_model_classified'
#
CREATE TABLE tx_mooxmarketplace_domain_model_classified (
  moox_marketplace_classifieds_type int(11) DEFAULT '0' NOT NULL,
  moox_marketplace_classifieds_status int(11) DEFAULT '0' NOT NULL,
  moox_marketplace_classifieds_imprint_agb text NOT NULL,
  moox_marketplace_classifieds_price varchar(50) DEFAULT NULL,
  moox_marketplace_classifieds_price_additional_info int(11) DEFAULT '0' NOT NULL,
  moox_marketplace_classifieds_shipping varchar(50) DEFAULT NULL,
  moox_marketplace_classifieds_shipping_additional_info int(11) DEFAULT '0' NOT NULL,
  moox_marketplace_classifieds_prices_vat int(11) DEFAULT '0' NOT NULL,
  moox_marketplace_classifieds_contact varchar(255) DEFAULT NULL,
  moox_marketplace_classifieds_contact_mail varchar(255) DEFAULT NULL,
  moox_marketplace_classifieds_contact_city varchar(255) DEFAULT NULL,
  moox_marketplace_classifieds_contact_zip varchar(255) DEFAULT NULL,
  moox_marketplace_classifieds_contact_street varchar(255) DEFAULT NULL,
  moox_marketplace_classifieds_contact_telephone varchar(255) DEFAULT NULL,
	moox_marketplace_classifieds_files int(11) unsigned DEFAULT '0' NOT NULL
);